package subproject;

public class DemoSubprojectCopy {

	public static void main(String[] args) {
		int k_subproject;
	}
	
	public boolean unusedMethodSubproject(boolean k) {
		k = true;
		
        String foo = null;
        System.out.println(foo.length());
        
        return k;
	}
	
	public class Foo {
    /*volatile */ Object baz = null; // fix for Java5 and later: volatile
    Object bar() {
        if (baz == null) { // baz may be non-null yet not fully created
            synchronized(this) {
                if (baz == null) {
                    baz = new Object();
                }
              }
        }
        return baz;
    }
}
}
